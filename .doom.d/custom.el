(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auth-source-save-behavior nil)
 '(calendar-date-style 'european)
 '(calendar-iso-date-display-form t)
 '(custom-safe-themes
   '("e3daa8f18440301f3e54f2093fe15f4fe951986a8628e98dcd781efbec7a46f2" default))
 '(elfeed-feeds
   '(("https://thepressproject.gr/feed/" News)
     ("https://eclass.hmu.gr/modules/announcements/rss.php?c=TM198&uid=24574&token=42dc52d859eb87d417fd6a7b3d2e2450aa9a693d" school Mech design)
     ("https://eclass.hmu.gr/modules/announcements/rss.php?c=TM145&uid=24574&token=16d45cc331205f36305d68aed90f9d626fe9aca5" school Managment)
     ("https://eclass.hmu.gr/modules/announcements/rss.php?c=MECH215&uid=24574&token=87e7d783df05b4ccecdc887abbb00a2b9307fbcf" school Thermodynamics)
     ("https://eclass.hmu.gr/modules/announcements/rss.php?c=MECH141&uid=24574&token=6a8748b41b6d6c56d21f0cad402db48fbef345bb" school Electronix)
     ("https://opensource.com/feed" linux)))
 '(elfeed-goodies/entry-pane-position 'bottom)
 '(elfeed-goodies/powerline-default-separator nil)
 '(elfeed-search-filter "@2-week-ago ")
 '(elfeed-show-entry-delete 'elfeed-kill-buffer)
 '(elfeed-show-entry-switch 'switch-to-buffer)
 '(elfeed-web-enabled t)
 '(org-modules '(ol-bibtex org-habit))
 '(package-selected-packages
   '(gnuplot-mode gnuplot consult-eglot eglot yasnippet-snippets py-snippets elfeed-web afternoon-theme deadgrep projectile transient rgb rg projectile-ripgrep ripgrep elfeed-goodies elfeed all-the-icons howm calfw-howm org-journal org-fragtog visual-fill-column svg rainbow-mode python-pytest python-mode python preview-dvisvgm ox-gemini org-superstar org-roam org-present org-latex-impatient ob-latex-as-png ob-ipython neotree math-preview latex-math-preview jupyter evil-paredit eshell-vterm emms cdlatex)))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(dap-mouse-eval-thing-face ((t (:background "grey10" :box (:line-width (1 . -1) :color "#2aa1ae")))))
 '(markdown-header-face ((t (:inherit font-lock-function-name-face :weight bold :family "variable-pitch"))))
 '(markdown-header-face-1 ((t (:inherit markdown-header-face :height 1.8))))
 '(markdown-header-face-2 ((t (:inherit markdown-header-face :height 1.4))))
 '(markdown-header-face-3 ((t (:inherit markdown-header-face :height 1.2)))))
(put 'customize-variable 'disabled nil)
(put 'projectile-ripgrep 'disabled nil)
