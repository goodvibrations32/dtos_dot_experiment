;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Torosian Nikolas"
      user-mail-address "goodvibrations32@protonmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
(setq doom-font (font-spec :family "Source Code Pro" :size 15 :weight 'semi-light)
     doom-variable-pitch-font (font-spec :family "Ubuntu" :size 13)
     doom-big-font (font-spec :family "Source Code Pro" :size 24)
     )
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-gruvbox)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(beacon-mode 1)
;; (require 'neotree)
(map! :leader
      (:prefix ("b". "buffer")
       :desc "List bookmarks" "L" #'list-bookmarks
       :desc "Save current bookmarks to bookmark file" "w" #'bookmark-save))


(global-auto-revert-mode 1)
(setq global-auto-revert-non-file-buffers t)

;; I-buffer keybindings
(evil-define-key 'normal ibuffer-mode-map
  (kbd "f c") 'ibuffer-filter-by-content
  (kbd "f d") 'ibuffer-filter-by-directory
  (kbd "f f") 'ibuffer-filter-by-filename
  (kbd "f m") 'ibuffer-filter-by-mode
  (kbd "f n") 'ibuffer-filter-by-name
  (kbd "f x") 'ibuffer-filter-disable
  (kbd "g h") 'ibuffer-do-kill-lines
  (kbd "g H") 'ibuffer-update)



(map! :leader
      (:prefix ("c h" . "Help info from Clippy")
       :desc "Clippy describes function under point" "f" #'clippy-describe-function
       :desc "Clippy describes variable under point" "v" #'clippy-describe-variable))


;; Dashboard customization for my needs with custom logo ;D
(use-package dashboard
  :init      ;; tweak dashboard config before loading it
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq font-lock-maximum-decoration t)
  ;; (setq dashboard-startup-banner 'logo) ;; use standard emacs logo as banner
  ;; (setq dashboard-startup-banner "~/.doom.d/doom-emacs-dash-dark-gruv.png")  ;; use custom image as banner
  (setq dashboard-startup-banner "~/.doom.d/doom-emacs-dash.png")  ;; use custom image as banner
  (setq dashboard-center-content t) ;; set to 't' for centered content
  (setq dashboard-items '(
                          (projects . 8)
                          (bookmarks . 9)
                          (recents . 8)
                          (registers . 5)
                          ))
  :config
  (dashboard-setup-startup-hook)
  (dashboard-modify-heading-icons '((recents . "file-text")
                                    (bookmarks . "book"))))



(setq doom-fallback-buffer-name "*dashboard*")


(map! :leader
      (:prefix ("d" . "dired")
       :desc "Open dired" "d" #'dired
       :desc "Dired jump to current" "j" #'dired-jump)
      (:after dired
       (:map dired-mode-map
        :desc "Peep-dired image previews" "d p" #'peep-dired
        :desc "Dired view file" "d v" #'dired-view-file)))

;; keybinds for dired to navigate with hjkl and basic file manipulation
(evil-define-key 'normal dired-mode-map
  (kbd "M-RET") 'dired-display-file
  (kbd "h") 'dired-up-directory
  (kbd "l") 'dired-find-file ; use dired-find-file instead of dired-open.
  (kbd "m") 'dired-mark
  (kbd "t") 'dired-toggle-marks
  (kbd "u") 'dired-unmark
  (kbd "C") 'dired-do-copy
  (kbd "D") 'dired-do-delete
  (kbd "J") 'dired-goto-file
  (kbd "M") 'dired-do-chmod
  (kbd "O") 'dired-do-chown
  (kbd "P") 'dired-do-print
  (kbd "R") 'dired-do-rename
  (kbd "T") 'dired-do-touch
  (kbd "Y") 'dired-copy-filenamecopy-filename-as-kill ; copies filename to kill ring.
  (kbd "+") 'dired-create-directory
  (kbd "-") 'dired-up-directory
  (kbd "% l") 'dired-downcase
  (kbd "% u") 'dired-upcase
  (kbd "; d") 'epa-dired-do-decrypt
  (kbd "; e") 'epa-dired-do-encrypt)
;; Get file icons in dired
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
;; With dired-open plugin, you can launch external programs for certain extensions
;; For example, I set all .png files to open in 'sxiv' and all .mp4 files to open in 'mpv'
(setq dired-open-extensions '(("gif" . "sxiv")
                              ("jpg" . "sxiv")
                              ("png" . "sxiv")
                              ("mkv" . "mpv")
                              ("mp4" . "mpv")))


(evil-define-key 'normal peep-dired-mode-map
  (kbd "j") 'peep-dired-next-file
  (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)

;; (emms-all)
;; (emms-default-players)
;; (emms-mode-line 1)
;; (emms-playing-time 1)
;; (setq emms-source-file-default-directory "~/Music/"
;;       emms-playlist-buffer-name "*Music*"
;;       emms-info-asynchronously t
;;       emms-source-file-directory-tree-function 'emms-source-file-directory-tree-find)
;; (map! :leader
;;       (:prefix ("1" . "EMMS audio player")
;;        :desc "Go to emms playlist" "a" #'emms-playlist-mode-go
;;        :desc "Emms pause track" "x" #'emms-pause
;;        :desc "Emms stop track" "s" #'emms-stop
;;        :desc "Emms play previous track" "p" #'emms-previous
;;        :desc "Emms play next track" "n" #'emms-next))

(setq delete-by-moving-to-trash t
      trash-directory "~/.local/share/Trash/files/")

;; usefull to navigate in the document ;D (G > line number)
(setq display-line-numbers-type t)

;;map some toggle operations to SPC t ...
(map! :leader
      :desc "Comment or uncomment lines" "TAB TAB" #'comment-line
      (:prefix ("t" . "toggle")
       :desc "Toggle line numbers" "l" #'doom/toggle-line-numbers
       :desc "Toggle line highlight in frame" "h" #'hl-line-mode
       :desc "Toggle line highlight globally" "H" #'global-hl-line-mode
       :desc "Toggle truncate lines" "t" #'toggle-truncate-lines
       :desc "Ripgrep consult" "r" #'consult-ripgrep
       :desc "Toogle treemacs" "d" #'treemacs))

;;custom faces for markdown files
(custom-set-faces
 '(markdown-header-face ((t (:inherit font-lock-function-name-face :weight bold :family "variable-pitch"))))
 '(markdown-header-face-1 ((t (:inherit markdown-header-face :height 1.8))))
 '(markdown-header-face-2 ((t (:inherit markdown-header-face :height 1.4))))
 '(markdown-header-face-3 ((t (:inherit markdown-header-face :height 1.2))))
 )

;; some nice collors from distrotube for gruvbox when i want to get fancy
(defun dt/org-colors-gruvbox-dark ()
  "Enable Gruvbox Dark colors for Org headers."
  (interactive)
  (dolist
      (face
       '((org-level-1 1.7 "#458588" ultra-bold)
         (org-level-2 1.6 "#b16286" extra-bold)
         (org-level-3 1.5 "#98971a" bold)
         (org-level-4 1.4 "#fb4934" semi-bold)
         (org-level-5 1.3 "#83a598" normal)
         (org-level-6 1.2 "#d3869b" normal)
         (org-level-7 1.1 "#d79921" normal)
         (org-level-8 1.0 "#8ec07c" normal)))
    (set-face-attribute (nth 0 face) nil :font doom-variable-pitch-font :weight (nth 3 face) :height (nth 1 face) :foreground (nth 2 face)))
    (set-face-attribute 'org-table nil :font doom-font :weight 'normal :height 1.0 :foreground "#bfafdf"))

;; map the headers to be toggled via the toggle sub-keybinds
(map! :leader
      :desc "Toogle pretty headers" "t P" #'dt/org-colors-gruvbox-dark)

;; (after! neotree
;;   (setq neo-smart-open t
;;         neo-window-fixed-size nil))
;; (after! doom-themes
;;   (setq doom-neotree-enable-variable-pitch t))
;; (map! :leader
;;       :desc "Toggle neotree file viewer" "t n" #'neotree-toggle
;;       :desc "Open directory in neotree" "d n" #'neotree-dir)

;; Org-mode setup for agenda capture-templates and more
(map! :leader
      :desc "Org babel tangle" "m B" #'org-babel-tangle)
(after! org
  (setq org-directory "~/org/"
        org-agenda-files '("~/org/agenda.org")
        org-default-notes-file (expand-file-name "notes.org" org-directory)
        org-ellipsis " ▼ "
        org-superstar-headline-bullets-list '("◉" "●" "○" "◆" "●" "○" "◆")
        org-superstar-item-bullet-alist '((?+ . ?➤) (?- . ?✦)) ; changes +/- symbols in item lists
        org-log-done 'time
        org-hide-emphasis-markers t
        ;; ex. of org-link-abbrev-alist in action
        ;; [[arch-wiki:Name_of_Page][Description]]
        org-link-abbrev-alist    ; This overwrites the default Doom org-link-abbrev-list
          '(("google" . "http://www.google.com/search?q=")
            ("arch-wiki" . "https://wiki.archlinux.org/index.php/")
            ("ddg" . "https://duckduckgo.com/?q=")
            ("wiki" . "https://en.wikipedia.org/wiki/"))
        org-table-convert-region-max-lines 20000
        org-todo-keywords        ; This overwrites the default Doom org-todo-keywords
          '((sequence
             "TODO(t)"           ; A task that is ready to be tackled
             "UNIV(u)"           ; Blog writing assignments
             "IDEA(i)"            ; Things to accomplish at the gym
             "PROJ(p)"           ; A project that contains other tasks
             "HOME(h)"          ; Video assignments
             "WAIT(w)"           ; Something is holding up this task
             "MODS(m)"           ; This is for any module project I need separate
             "HACK(H)"           ; Additional things to tinker
             "FIXME(f)"          ; For anything needs more patching
             "ASK(?)"            ; This is for colaboration projects
             "LOOP(l)"           ; Here is a tag for stuff on a loop
             "KILL(k)"           ; Killing a project
             ";TH;(T)"           ; Adding theory tag for projects
             "/EX/(x)"           ; Examples for any kind of project
             "DSPF(f)"           ; Filtering and digital signal processing
             "|"                 ; The pipe necessary to separate "active" states and "inactive" states
             "DONE(d)"           ; Task has been completed
             "CANCELLED(c)" )))  ; Task has been cancelled
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-into-drawer t)
  (setq org-agenda-include-diary t)
  (setq org-agenda-block-separator 126 )
  (setq org-agenda-custom-commands
        '(("h" "tasks Home-related"
           ((tags-todo "home")
            (tags-todo "voula")
            (tags "garden")
            ))
          ("d" "Upcoming deadlines" agenda ""
           ((org-agenda-time-grid nil)
            (org-deadline-warning-days 365)        ;; [1]
            (org-agenda-entry-types '(:deadline))  ;; [2]
            ))
          ("v" "Big agenda view"
           ((tags "PRIORITY=\"A\""
                  ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                   (org-agenda-overriding-header "High-priority unfinished tasks:")))
            (tags-todo "home")
            (tags-todo "voula")
            (tags-todo "exams")
            (agenda "")
            ;; (setq (org-agenda-day-view t))
            (tags "garden")
            (tags "PRIORITY=\"B\""
                  ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                   (org-agenda-overriding-header "Medium-priority unfinished tasks:")))
            (tags "PRIORITY=\"C\""
                  ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                   (org-agenda-overriding-header "Low-priority unfinished tasks:")))
            (tags-todo "Wflow")
            (tags-todo "meetings")
            (tags "office")
            ))
      ;; other commands go here
          ;; ("%" "Appointments" agenda* "Today's appointments"
          ;;  ((org-agenda-span 10)
          ;;   (org-agenda-start-on-weekday nil)
          ;;   (org-agenda-max-entries 15)))
          ;; ("m" "Meetings" agenda* "priority meettings"
          ;;  ((tags "meetings")
          ;;  ))
          ("w" "tasks Work-related"
           ((tags-todo "Wflow")
            (tags-todo "exams")
            (tags-todo "meetings")
            (tags "office")))
          ))
  (setq org-habit-following-days 7
        org-habit-preceding-days 35
        org-habit-show-habits t
        org-habit-show-all-today t
        org-habit-graph-column 90
 ;; (org-habit-alert-face :background yellow)
 ;; (org-habit-alert-future-face :background yellow)
 ;; (org-habit-clear-face :background blue)
 ;; (org-habit-clear-future-face :background blue)
 ;; (org-habit-overdue-face :background red)
 ;; (org-habit-overdue-future-face :background red)
 ;; (org-habit-ready-face :background green)
 ;; (org-habit-ready-future-face :background green)

  )
  (with-eval-after-load 'org-superstar
  (set-face-attribute 'org-superstar-item nil :height 1.2)
  (set-face-attribute 'org-superstar-header-bullet nil :height 1.2)
  (set-face-attribute 'org-superstar-leading nil :height 1.3))
  ;; Set different bullets, with one getting a terminal fallback.

  (setq org-superstar-headline-bullets-list '("◉" ("◈" ?◈) "○" "▷"))
  ;; Stop cycling bullets to emphasize hierarchy of headlines.
  (setq org-superstar-cycle-headline-bullets nil)
  ;; Hide away leading stars on terminal.
  (setq org-superstar-leading-fallback ?\s)

)


(setq org-capture-templates
      `(("t" "tasks / projects")
       ("tt" "task" entry (file+olp "~/org/agenda.org")
                "* TODO %?\n %U\n %a\n %i" :empty-lines 1)
       ("tp" "projects" entry (file+olp "~/org/agenda.org")
                "* Proj  %?\n %U\n %a\n %i" :empty-lines 1)


       ("h" "habits")
       ("hh" "Habit template" entry
        (file+olp+datetree "~/org/agenda.org")
        "* TODO %?
        :PROPERTIES:
        :STYLE: habit
        :LOGBOOK:
        :END:")


       ("j" "Journal Entries")
       ("jj" "Journal" entry
        (file+olp+datetree "~/nt/Org/journal/journal.org")
        "\n* %<I%H:%M %p> - Journal %a :journal:\n\n%?\n\n"
        :clock-in :clock-resume
        :empty-lines 1)

       ("jm" "Meeting" entry
        (file+olp+datetree "~/org/agenda.org")
        "\n* %<I%H:%M %p> - With :meetings: \n\n%?\n\n"
        :clock-in :clock-resume
        :empty-lines 1)

       ("w" "Workflows")
       ("wd" "Dissertation" entry
        (file+olp+datetree "~/org/agenda.org")
        "\n* %<I%H:%M %p> - %a :Wflow:\n\n%?\n\n"
        :clock-in :clock-resume
        :empty-lines 1)
       ("wn" "Dissertation" entry
        (file+olp+datetree "~/Documents/dissertation/org-docs/dissertation document/notes-diss.org")
        "\n* %<I%H:%M %p> - %a :Wflow:\n\n%?\n\n"
        :empty-lines 1)
       ("wg" "general work" entry
        (file+olp+datetree "~/org/agenda.org")
        "\n* %<I%H:%M %p> - %a :Wflow:\n\n%?\n\n"
        :clock-in :clock-resume
        :empty-lines 1)
       ("m" "Metrics Capture")
       ("mw" "Workout" table-line (file+headline "~/org/metrics.org" "Workout")
        "| %U | %^{time} | %^{notes}" :kill-buffer t)))


(setq org-refile-targets
      '(("~/org/archive.org" :maxlevel . 1)))
(advice-add 'org-refile :after 'org-save-all-org-buffers)


;; (use-package ox-gemini)
;; (use-package ox-man)


(setq org-journal-dir "~/nt/Org/journal/"
      org-journal-date-prefix "* "
      org-journal-time-prefix "** "
      ;; org-journal-date-format "%d %B, %Y (%A) "
      org-journal-file-format "%Y-%m-%d.org")
(map! :leader
      (:prefix ("j" . "journal") ;; org-journal bindings
        :desc "Create new journal entry" "j" #'org-journal-new-entry
        :desc "Open previous entry" "p" #'org-journal-previous-entry
        :desc "Open next entry" "n" #'org-journal-next-entry
        :desc "Search journal" "s" #'org-journal-search-forever))

;; The built-in calendar mode mappings for org-journal
;; conflict with evil bindings
(map!
 (:map calendar-mode-map
   :n "o" #'org-journal-display-entry
   :n "p" #'org-journal-previous-entry
   :n "n" #'org-journal-next-entry
   :n "O" #'org-journal-new-date-entry))

;; (setq diary-date-forms (format-time-string "%F"))
;; same as above with other variable
;; (setq diary-iso-date-forms 1)
(map!
 (:map diary-mode-map
       :n "D" #'diary-insert-entry-1))

;; Local leader (<SPC m>) bindings for org-journal in calendar-mode
;; I was running out of bindings, and these are used less frequently
;; so it is convenient to have them under the local leader prefix
(map!
 :map (calendar-mode-map)
 :localleader
 "w" #'org-journal-search-calendar-week
 "m" #'org-journal-search-calendar-month
 "y" #'org-journal-search-calendar-year)
(require 'calfw-cal)
(require 'calfw-ical)
(require 'calfw-howm)
(require 'calfw-org)

;; a more big calendar for displaying the tasks and appointments
(defun my-open-calendar ()
  (interactive)
  (cfw:open-calendar-buffer
   :contents-sources
   (list
    (cfw:org-create-source "Green")  ; orgmode source
    (cfw:howm-create-source "Blue")  ; howm source
    (cfw:cal-create-source "Orange") ; diary source
   )))

(setq org-publish-use-timestamps-flag nil)
(setq org-export-with-broken-links t)


(map! :leader
      :desc "Switch to perspective NAME" "deletechar" #'persp-switch
      :desc "Switch to buffer in perspective" "," #'persp-switch-to-buffer
      :desc "Switch to next perspective" "]" #'persp-next
      :desc "Switch to previous perspective" "[" #'persp-prev
      :desc "Add a buffer current perspective" "+" #'persp-add-buffer
      :desc "Remove perspective by name" "-" #'persp-remove-by-name)


;; (define-globalized-minor-mode global-rainbow-mode rainbow-mode
;;   (lambda () (rainbow-mode 1)))
;; (global-rainbow-mode 1 )



(map! :leader
      (:prefix ("r" . "registers")
       :desc "Copy to register" "c" #'copy-to-register
       :desc "Frameset to register" "f" #'frameset-to-register
       :desc "Insert contents of register" "i" #'insert-register
       :desc "Jump to register" "j" #'jump-to-register
       :desc "List registers" "l" #'list-registers
       :desc "Number to register" "n" #'number-to-register
       :desc "Interactively choose a register" "r" #'counsel-register
       :desc "View a register" "v" #'view-register
       :desc "Window configuration to register" "w" #'window-configuration-to-register
       :desc "Increment register" "+" #'increment-register
       :desc "Point to register" "SPC" #'point-to-register))


(setq shell-file-name "/bin/fish"
      vterm-max-scrollback 5000)
(setq eshell-rc-script "~/.config/doom/eshell/profile"
      eshell-aliases-file "~/.config/doom/eshell/aliases"
      eshell-history-size 5000
      eshell-buffer-maximum-lines 5000
      eshell-hist-ignoredups t
      eshell-scroll-to-bottom-on-input t
      eshell-destroy-buffer-when-process-dies t
      eshell-visual-commands'("bash" "fish" "htop" "ssh" "top" "zsh"))
(map! :leader
      :desc "Eshell" "e s" #'eshell
      :desc "Eshell popup toggle" "e t" #'+eshell/toggle
      :desc "Counsel eshell history" "e h" #'counsel-esh-history
      :desc "Vterm popup toggle" "v t" #'+vterm/toggle)

(setq visual-fill-column-width 110
      visual-fill-column-center-text t)
(defun org-present-start ()
  (setq-local face-remapping-alist'((default (:height 1.85) variable-pitch)
                                    (header-line (:height 8.0) variable-pitch)
                                    (org-document-title (:height 1.95) org-document-title)
                                    (org-code (:height 1.75) org-code)
                                    (org-verbatim (:height 1.75) org-verbatim)
                                    (org-block (:height 1.45) org-block)
                                    (org-block-begin-line (:height 0.7) org-block)))

  (beacon-mode 0)
  (setq line-number-mode nil)
  ;; (doom/toggle-line-numbers nil)
  (setq header-line-format "  ")
  (visual-fill-column-mode 1)
  (visual-line-mode 1)
  ;; (set-frame-parameter (selected-frame) 'alpha '(97 . 100))
  ;; (add-to-list 'default-frame-alist '(alpha . (90 . 90))
)

(defun org-present-stop ()
  (setq-local face-remapping-alist '((default variable-pitch default)))
  (setq header-line-format nil)
  (visual-fill-column-mode 0)
  (visual-line-mode 0)
  (beacon-mode 1)
  (setq line-number-mode t)
  ;; (doom/toggle-line-numbers t)
  ;; (set-frame-parameter (selected-frame) 'alpha '(100 . 100))
  ;; (add-to-list 'default-frame-alist '(alpha . (100 . 100))
)

(add-hook 'org-present-mode-hook 'my/org-present-start)
(add-hook 'org-present-mode-quit-hook 'my/org-present-stop)


(defun prefer-horizontal-split ()
  (set-variable 'split-height-threshold nil t)
  (set-variable 'split-width-threshold 40 t)) ; make this as low as needed

(add-hook 'markdown-mode-hook 'prefer-horizontal-split)

(map! :leader
      :desc "Clone indirect buffer other window" "b c" #'clone-indirect-buffer-other-window)


(map! :leader
      (:prefix ("w" . "window")
       :desc "Winner redo" "<right>" #'winner-redo
       :desc "Winner undo" "<left>" #'winner-undo))

;; Resize windows without hasle
;; Vertical resize in each direction via +/-
(defun v-resize (key)
 "interactively resize the window"
 (interactive "cHit +/- to verdical enlarge/shrink")
   (cond
     ((eq key (string-to-char "+"))
        (enlarge-window 1)
        (call-interactively 'v-resize))
     ((eq key (string-to-char "-"))
        (enlarge-window -1)
        (call-interactively 'v-resize))
     (t (push key unread-command-events))))
(map! :leader
      (:prefix ("w" . "window")
      :desc "Resize window (+/-) vertical" "2" #'v-resize))

;; Horizontal window resize in each direction via +/-
(defun h-resize (key)
 "interactively resize the window"
 (interactive "cHit +/- to horizontal enlarge/shrink")
   (cond
     ((eq key (string-to-char "+"))
        (enlarge-window-horizontally 1)
        (call-interactively 'h-resize))
     ((eq key (string-to-char "-"))
        (enlarge-window-horizontally -1)
        (call-interactively 'h-resize))
     (t (push key unread-command-events))))
(map! :leader
      (:prefix ("w" . "window")
      :desc "Resize window (+/-) horizontal" "1" #'h-resize))



(setq org-babel-default-header-args:jupyter-typescript '(
                                                         (:session . "ts")
                                                         (:kernel . "tslab")))
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t) ;; Other languages
   (shell . t)
   (gnuplot . t)
   ;; Python & Jupyter
   (python . t)
   (jupyter . t)))

(map! :leader
      (:prefix ("c")
             :desc "Insert Code Blocks" "b" #'org-insert-structure-template
             :desc "Execute jupyter block and move to next" "j" #'jupyter-org-execute-and-next-block))



;; this seems to add syntax-highlighting to jupyter-python and jupyter-typescript blocks
(after! org-src
 (dolist (lang '(python typescript jupyter))
 (cl-pushnew (cons (format "jupyter-%s" lang) lang)
                org-src-lang-modes :key #'car))
 )



(setq char-or-string-p t)
(setq org-clock-sound "/opt/dtos-sounds/shutdown-01.mp3")


(blink-cursor-mode 1)

(with-eval-after-load 'org
  (add-to-list 'org-modules 'org-habit t)
  (add-to-list 'org-modules 'org-secretary t))
;;
(use-package! org-fragtog-mode
:after org
:hook (org-mode . org-fragtog-mode) ; this auto-enables it when you enter an org-buffer, remove if you do not want this
:config
;; whatever you want
)
(setq org-format-latex-options (plist-put org-format-latex-options :scale 1.85))

(global-ede-mode 1)
(require 'semantic/sb)
(semantic-mode 1)


;; (advice-add 'org-create-formula-image :around #'org-renumber-environment)
;; If non-nil, cause imenu to see `doom-modeline' declarations.
;; This is done by adjusting `lisp-imenu-generic-expression' to
;; include support for finding `doom-modeline-def-*' forms.
;; Must be set before loading doom-modeline.
(setq doom-modeline-support-imenu t)

;; How tall the mode-line should be. It's only respected in GUI.
;; If the actual char height is larger, it respects the actual height.
(setq doom-modeline-height 25)

;; How wide the mode-line bar should be. It's only respected in GUI.
(setq doom-modeline-bar-width 4)

;; Whether to use hud instead of default bar. It's only respected in GUI.
(setq doom-modeline-hud t)

;; The limit of the window width.
;; If `window-width' is smaller than the limit, some information won't be
;; displayed. It can be an integer or a float number. `nil' means no limit."
(setq doom-modeline-window-width-limit 85)

;; How to detect the project root.
;; nil means to use `default-directory'.
;; The project management packages have some issues on detecting project root.
;; e.g. `projectile' doesn't handle symlink folders well, while `project' is unable
;; to hanle sub-projects.
;; You can specify one if you encounter the issue.
(setq doom-modeline-project-detection 'auto)

;; Determines the style used by `doom-modeline-buffer-file-name'.
;;
;; Given ~/Projects/FOSS/emacs/lisp/comint.el
;;   auto => emacs/l/comint.el (in a project) or comint.el
;;   truncate-upto-project => ~/P/F/emacs/lisp/comint.el
;;   truncate-from-project => ~/Projects/FOSS/emacs/l/comint.el
;;   truncate-with-project => emacs/l/comint.el
;;   truncate-except-project => ~/P/F/emacs/l/comint.el
;;   truncate-upto-root => ~/P/F/e/lisp/comint.el
;;   truncate-all => ~/P/F/e/l/comint.el
;;   truncate-nil => ~/Projects/FOSS/emacs/lisp/comint.el
;;   relative-from-project => emacs/lisp/comint.el
;;   relative-to-project => lisp/comint.el
;;   file-name => comint.el
;;   buffer-name => comint.el<2> (uniquify buffer name)
;;
;; If you are experiencing the laggy issue, especially while editing remote files
;; with tramp, please try `file-name' style.
;; Please refer to https://github.com/bbatsov/projectile/issues/657.
(setq doom-modeline-buffer-file-name-style 'truncate-upto-root)

;; Whether display icons in the mode-line.
;; While using the server mode in GUI, should set the value explicitly.
(setq doom-modeline-icon t)

;; Whether display the icon for `major-mode'. It respects `doom-modeline-icon'.
(setq doom-modeline-major-mode-icon t)

;; Whether display the colorful icon for `major-mode'.
;; It respects `all-the-icons-color-icons'.

(setq all-the-icons-color-icons t)
(setq doom-modeline-major-mode-color-icon t)

;; Whether display the icon for the buffer state. It respects `doom-modeline-icon'.
(setq doom-modeline-buffer-state-icon t)

;; Whether display the modification icon for the buffer.
;; It respects `doom-modeline-icon' and `doom-modeline-buffer-state-icon'.
(setq doom-modeline-buffer-modification-icon t)

;; Whether display the time icon. It respects variable `doom-modeline-icon'.
(setq doom-modeline-time-icon t)

;; Whether to use unicode as a fallback (instead of ASCII) when not using icons.
(setq doom-modeline-unicode-fallback nil)

;; Whether display the buffer name.
(setq doom-modeline-buffer-name t)

;; Whether display the minor modes in the mode-line.
(setq doom-modeline-minor-modes nil)

;; If non-nil, a word count will be added to the selection-info modeline segment.
(setq doom-modeline-enable-word-count nil)

;; Major modes in which to display word count continuously.
;; Also applies to any derived modes. Respects `doom-modeline-enable-word-count'.
;; If it brings the sluggish issue, disable `doom-modeline-enable-word-count' or
;; remove the modes from `doom-modeline-continuous-word-count-modes'.
(setq doom-modeline-continuous-word-count-modes '(markdown-mode gfm-mode org-mode))

;; Whether display the buffer encoding.
(setq doom-modeline-buffer-encoding t)

;; Whether display the indentation information.
(setq doom-modeline-indent-info nil)

;; If non-nil, only display one number for checker information if applicable.
(setq doom-modeline-checker-simple-format t)

;; The maximum number displayed for notifications.
(setq doom-modeline-number-limit 99)

;; The maximum displayed length of the branch name of version control.
(setq doom-modeline-vcs-max-length 12)

;; Whether display the workspace name. Non-nil to display in the mode-line.
(setq doom-modeline-workspace-name t)

;; doom-battery-try
;; (setq display-battery-mode t)
;; (setq doom-modeline--battery-status t)

;; Whether display the perspective name. Non-nil to display in the mode-line.
(setq doom-modeline-persp-name t)

;; If non nil the default perspective name is displayed in the mode-line.
(setq doom-modeline-display-default-persp-name t)

;; If non nil the perspective name is displayed alongside a folder icon.
(setq doom-modeline-persp-icon nil)

;; Whether display the `lsp' state. Non-nil to display in the mode-line.
(setq doom-modeline-lsp t)

;; Whether display the GitHub notifications. It requires `ghub' package.
(setq doom-modeline-github nil)

;; The interval of checking GitHub.
(setq doom-modeline-github-interval (* 30 60))

;; Whether display the modal state.
;; Including `evil', `overwrite', `god', `ryo' and `xah-fly-keys', etc.
(setq doom-modeline-modal t)

;; Whether display the modal state icon.
;; Including `evil', `overwrite', `god', `ryo' and `xah-fly-keys', etc.
(setq doom-modeline-modal-icon t)

;; ;; Whether display the mu4e notifications. It requires `mu4e-alert' package.
;; (setq doom-modeline-mu4e nil)
;; ;; also enable the start of mu4e-alert
;; (mu4e-alert-enable-mode-line-display)

;; Whether display the gnus notifications.
(setq doom-modeline-gnus t)

;; Whether gnus should automatically be updated and how often (set to 0 or smaller than 0 to disable)
(setq doom-modeline-gnus-timer 2)

;; Wheter groups should be excludede when gnus automatically being updated.
(setq doom-modeline-gnus-excluded-groups '("dummy.group"))

;; Whether display the IRC notifications. It requires `circe' or `erc' package.
(setq doom-modeline-irc t)

;; Function to stylize the irc buffer names.
(setq doom-modeline-irc-stylize 'identity)

;; Whether display the time. It respects `display-time-mode'.
(setq doom-modeline-time t)

;; Whether display the misc segment on all mode lines.
;; If nil, display only if the mode line is active.
(setq doom-modeline-display-misc-in-all-mode-lines t)

;; Whether display the environment version.
(setq doom-modeline-env-version t)
;; Or for individual languages
(setq doom-modeline-env-enable-python t)
(setq doom-modeline-env-enable-ruby t)
(setq doom-modeline-env-enable-perl t)
(setq doom-modeline-env-enable-go t)
(setq doom-modeline-env-enable-elixir t)
(setq doom-modeline-env-enable-rust t)

;; Change the executables to use for the language version string
(setq doom-modeline-env-python-executable "python") ; or `python-shell-interpreter'
(setq doom-modeline-env-ruby-executable "ruby")
(setq doom-modeline-env-perl-executable "perl")
(setq doom-modeline-env-go-executable "go")
(setq doom-modeline-env-elixir-executable "iex")
(setq doom-modeline-env-rust-executable "rustc")

;; What to display as the version while a new one is being loaded
(setq doom-modeline-env-load-string "...")

;; By default, almost all segments are displayed only in the active window. To
;; display such segments in all windows, specify e.g.
;; (setq doom-modeline-always-visible-segments '(mu4e irc))

;; Hooks that run before/after the modeline version string is updated
(setq doom-modeline-before-update-env-hook nil)
(setq doom-modeline-after-update-env-hook nil)
;; (use-package! elfeed-goodies)
;; (elfeed-goodies/setup)
;; (setq elfeed-goodies/entry-pane-size 0.5)
;; (add-hook 'elfeed-show-mode-hook 'visual-line-mode)
;; (evil-define-key 'normal elfeed-show-mode-map
;;   (kbd "J") 'elfeed-goodies/split-show-next
;;   (kbd "K") 'elfeed-goodies/split-show-prev)
;; (evil-define-key 'normal elfeed-search-mode-map
;;   (kbd "J") 'elfeed-goodies/split-show-next
;;   (kbd "K") 'elfeed-goodies/split-show-prev)
;; (defface important-elfeed-entry
;;   '((t :foreground "#f77"))
;;   "Marks an important Elfeed entry.")

;; (push '(important important-elfeed-entry)
;;       elfeed-search-face-alist)
(setq elfeed-feeds
      '(("https://www.techrepublic.com/rssfeeds/topic/open-source/")
        ("https://thepressproject.gr/feed/" Press news)
        ("https://opensource.com/feed" opensource linux)))
                    ;; (("https://athens.indymedia.org/feed/11/")
                     ;; ("https://www.reddit.com/r/commandline.rss" reddit commandline)
                     ;; ("https://www.reddit.com/r/distrotube.rss" reddit distrotube)
                     ;; ("https://www.reddit.com/r/emacs.rss" reddit emacs)
                     ;; ("https://www.gamingonlinux.com/article_rss.php" gaming linux)
                     ;; ("https://hackaday.com/blog/feed/" hackaday linux)
                     ;; ("https://opensource.com/feed" opensource linux)
                     ;; ("https://linux.softpedia.com/backend.xml" softpedia linux)
                     ;; ("https://itsfoss.com/feed/" itsfoss linux)
                     ;; ("https://www.zdnet.com/topic/linux/rss.xml" zdnet linux)
                     ;; ("https://www.phoronix.com/rss.php" phoronix linux)
                     ;; ("http://feeds.feedburner.com/d0od" omgubuntu linux)
                     ;; ("https://www.computerworld.com/index.rss" computerworld linux)
                     ;; ("https://www.networkworld.com/category/linux/index.rss" networkworld linux)

      ;; '"https://www.techrepublic.com/rssfeeds/topic/open-source/" techrepublic linux))
                     ;; ("https://betanews.com/feed" betanews linux)
                     ;; ("http://lxer.com/module/newswire/headlines.rss" lxer linux)
                     ;; ("https://distrowatch.com/news/dwd.xml" distrowatch linux)
;; (setq dap-auto-configure-mode t)
(require 'dap-python)
(require 'dap-cpptools)
;; (after! dap-mode
;;   (setq dap-python-debugger 'debugpy))
;; (dap-python-debugger 'debugpy)
;; (python-shell-interpreter "python3")
;; (dap-python-executable "python3")
;; ;; (dap-register-debug-template
;; ;;  "Python :: Run pytest"
;; ;;  (list :type "python"
;; ;;        :debugger "debugpy"))

;; custom image subtitle
(setq org-odt-category-map-alist
      '(("__Figure__" "Εικόνα" "value" "Figure" org-odt--enumerable-p)))

;; (setq calendar-date-display-form iso8601--date-match)
(setq calendar-latitude 35.3)
(setq calendar-longitude 25.1)
(setq calendar-mark-diary-entries-flag t)
(add-hook 'org-finalize-agenda-hook
  (lambda ()
    (setq appt-message-warning-time 10        ;; warn 10 min in advance
          appt-display-diary nil              ;; do not display diary when (appt-activate) is called
          appt-display-mode-line t            ;; show in the modeline
          appt-display-format 'window         ;; display notification in window
          calendar-mark-diary-entries-flag t) ;; mark diary entries in calendar
    (org-agenda-to-appt)                      ;; copy all agenda schedule to appointments
    (appt-activate 1)))                       ;; active appt (appointment notification)
